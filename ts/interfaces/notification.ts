export interface IMailgunNotification {
  timestamp: string;
  token: string;
  signature: string;
  domain: string;
  'Received-Spf': string;
  From: string;
  'Return-Path': string;
  'Arc-Seal': string[];
  'Delivered-To': string;
  'X-Google-Dkim-Signature': string;
  To: string;
  'Dkim-Signature': string;
  subject: string;
  from: string;
  'X-Received': string[];
  'Ironport-Sdr': string;
  'Arc-Authentication-Results': string[];
  'Arc-Message-Signature': string[];
  Date: string;
  'Message-Id': string;
  'Mime-Version': string;
  Received: string[];
  'Authentication-Results': string[];
  'X-Ipas-Result': string;
  'message-url': string;
  'message-headers': string;
  'Reply-To': string;
  recipient: string;
  sender: string;
  'X-Mailgun-Incoming': string;
  'X-Forwarded-For': string;
  'X-Gm-Message-State': string;
  'X-Google-Smtp-Source': string;
  'X-Envelope-From': string;
  'Content-Type': string;
  'X-Forwarded-To': string;
  Subject: string;
  attachments: string;
  'body-plain': string;
  'stripped-text': string;
  'stripped-html': string;
  'stripped-signature': string;

  // Lossless specific
  'X-Lossless-Auth': string;
}
