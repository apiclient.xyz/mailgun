import { expect, tap } from '@pushrocks/tapbundle';
import { Qenv } from '@pushrocks/qenv';
import * as smartmail from '@pushrocks/smartmail';

const testQenv = new Qenv('./', './.nogit');

import * as mailgun from '../ts/index.js';
import { IMailgunMessage } from '../ts/index.js';

let testMailgunAccount: mailgun.MailgunAccount;
let testSmartmail: smartmail.Smartmail<IMailgunMessage>;

tap.test('should create a mailgun account', async () => {
  testMailgunAccount = new mailgun.MailgunAccount({
    apiToken: testQenv.getEnvVarOnDemand('MAILGUN_API_TOKEN'),
    region: 'eu',
  });
  await testMailgunAccount.addSmtpCredentials(
    testQenv.getEnvVarOnDemand('MAILGUN_SMTP_CREDENTIALS')
  );
  expect(testMailgunAccount).toBeInstanceOf(mailgun.MailgunAccount);
});

tap.test('should create a smartmail', async () => {
  testSmartmail = new smartmail.Smartmail({
    body: 'hi there. This is the body.',
    from: 'Lossless GmbH <noreply@mail.lossless.com>',
    subject: 'TestMessage from @mojoio/mailgun test',
  });
  expect(testSmartmail).toBeInstanceOf(smartmail.Smartmail);
});

tap.test('should send a smartmail', async () => {
  await testMailgunAccount.sendSmartMail(testSmartmail, 'Sandbox Team <sandbox@mail.git.zone>');
});

tap.test('should send a smartmail with empty body', async () => {
  const emptyBodySmartmail = new smartmail.Smartmail<IMailgunMessage>({
    body: '',
    from: 'Lossless GmbH <noreply@mail.lossless.one>',
    subject: 'A message with no body from @mojoio/mailgun test',
  });
  await testMailgunAccount.sendSmartMail(
    emptyBodySmartmail,
    'Sandbox Team <sandbox@mail.git.zone>'
  );
});

tap.test('should retrieve a mail using a retrieval url', async () => {
  const result = await testMailgunAccount.retrieveSmartMailFromMessageUrl(
    'https://sw.api.mailgun.net/v3/domains/mail.lossless.one/messages/AgMFnnnAKC8xp_dDa79LyoxhloxtaVmnRA=='
  );
  console.log(result);

  // TODO handle empty body
  if (false) {
    result.options.subject = 'hi there. This is a testmail with attachment';
    result.options.from = 'noreply@mail.lossless.com';
    await testMailgunAccount.sendSmartMail(result, 'Sandbox Team <sandbox@mail.git.zone>');
  }
});

tap.start();
